// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {

  const sequelizeClient = app.get('sequelizeClient');
  const user = sequelizeClient.define('user', {
      id: {
        fields: ['id'],
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      paswd: {
        type: DataTypes.STRING,
        allowNull: false
      },
      created_at: {
        fields: ['created_at'],
        type: 'TIMESTAMP',
      },
      updated_at: {
        fields: ['updated_at'],
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      }

    }, {
      timestamps  : true,
      underscored : true,
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    });

  // eslint-disable-next-line no-unused-vars
  user.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return user;
};
